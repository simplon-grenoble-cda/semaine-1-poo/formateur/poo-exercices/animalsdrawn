## Fonctionnement du programme

Programme en ligne de commande, on demande à l'utilisateur s'il veut rajouter un animal à son troupeau


```
Quel animal voulez vous ajouter à votre troupeau (vache/cheval/chameau/rien) ?
```


Cette invite se répète en boucle. Si l'utilisateur choisi une option valide (vache, cheval, ou chameau) on rajoute
un animal de ce type au troupeau, et on pose à nouveau la question.

Si l'utilisateur choisi "rien", on passe à la suite.

On fait en sorte de bien afficher un récapitulatif du troupeau actuel à chaque fois que l'invite est présentée 
à nouveau à l'utilisateur.

Une fois que l'utilisateur a fini de composer son troupeau et passe à la suite, on lui affiche en ASCII art
une représentation de son troupeau. Par exemple pour 2 vaches et 1 cheval : 


```
         (__)    
 `\------(oo)
   ||    (__)
   ||w--||   



         (__)    
 `\------(oo)
   ||    (__)
   ||w--||   

           ,--,
     _ ___/ /\|
 ,;'( )__, )  ~
//  //   '--; 
'   \     | ^
     ^    ^
```

Les animaux doivent apparaître dans l'ordre où ils ont été insérés.


## Implémentation

Créer une classe Animal, et des classes filles Cow, Horse et Camel.

La classe Animal doit avoir une méthode `draw()` qui ne fait rien

Les classes filles doivent surcharger la méthode `draw()` pour afficher le asciiART de l'animal en question


On doit ensuite créer une classe `Herd` avec les méthodes suivantes :

- `addAnimal(Animal animal)` : ajoute un animal au troupeau
- `drawHerd()` : dessine à la console tous les animeaux du troupeau


Enfin, compléter le main existant pour avoir le programme comple qui fonctionne
